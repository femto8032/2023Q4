# Отчёт по квартальным целям Q4

## 1. Изучить распределённый программный брокер сообщений Apache Kafka и реализовать сервис с его использованием

### Введение

Apache Kafka – это распределенное хранилище данных, оптимизированное для приема и обработки потоковых данных в режиме реального времени. Потоковые данные – это данные, непрерывно генерируемые тысячами источников данных, которые, как правило, передают записи данных одновременно. Потоковая платформа должна справляться с таким постоянным притоком данных и обрабатывать их последовательно и поэтапно.

Kafka выполняет три основные функции:

- публикует потоки записей и подписывается на них;
- эффективно хранит потоки в том порядке, в котором они были созданы;
- обрабатывает потоки в реальном времени.

Kafka в основном используется для создания конвейеров потоковых данных в реальном времени и приложений, адаптированных к этим потокам. Система позволяет обмениваться сообщениями, обрабатывать потоки, а также хранить и анализировать как данные за прошедшие периоды, так и те, что поступают в реальном времени.

### Архитектура Kafka

Kafka управляет логами и организует платформу, которая соединяет поставщиков данных с потребителями и даёт возможность получать упорядоченный поток событий в реальном времени. Лог — это упорядоченный поток событий во времени. Событие происходит, попадает в конец лога и остаётся там неизменным.

Kafka состоит из трёх компонентов:

1. Сервер (Брокер)

    Кластер Kafka состоит из брокеров. Можно представить систему как дата-центр и серверы в нём. При первом знакомстве думайте о Kafka-брокере как о компьютере: это процесс в операционной системе с доступом к своему локальному диску. Все брокеры соединены друг с другом сетью и действуют сообща, образуя единый кластер. Когда мы говорим, что продюсеры пишут события в Kafka-кластер, то подразумеваем, что они работают с брокерами в нём.

2. Продюсеры

    Для записи событий в кластер Kafka есть продюсеры — это приложения, которые вы разрабатываете. Программа-продюсер записывает сообщение в Kafka, тот сохраняет события, возвращает подтверждение о записи или acknowledgement. Продюсер получает его и начинает следующую запись.

3. Консьюмеры — считывают эти сообщения, используя модель pull.

    События, которые продюсеры записывают на локальные диски брокеров, могут читать консумеры — это тоже приложения, которые вы разрабатываете. В этом случае кластер Kafka — это по-прежнему нечто, что обслуживается инфраструктурой, где вы как пользователь пишете продюсеров и консумеров. Программа-консумер подписывается на события или поллит и получает данные в ответ. Так продолжается по кругу.

![Базовые компоненты Kafka](https://habrastorage.org/webt/1m/tr/bb/1mtrbbxjbpilpcm2zilh8blz5t8.jpeg)

Логическое разделение категорий сообщений на группы называется **топик**. Например, события по статусам заказов, координат партнёров, маршрутных листов и так далее.

Топик удобно представлять как лог — вы пишете событие в конец и не разрушаете при этом цепочку старых событий. Общая логика:

- Один продюсер может писать в один или несколько топиков

- Один консумер может читать один или несколько топиков

- В один топик могут писать один или более продюсеров

- Из одного топика могут читать один или более консумеров

Топики в Kafka разделены на партиции. Увеличение партиций увеличивает параллелизм чтения и записи. Партиции находятся на одном или нескольких брокерах, что позволяет кластеру масштабироваться. Партиции хранятся на локальных дисках брокеров и представлены набором лог-файлов — сегментов. Запись в них идёт в конец, а уже сохранённые события неизменны.

Каждое сообщение в таком логе определяется порядковым номером — оффсетом. Этот номер монотонно увеличивается при записи для каждой партиции.

Лог-файлы на диске устаревают по времени или размеру. Настроить это можно глобально или индивидуально в каждом топике.

Для отказоустойчивости партиции могут реплицироваться. Число реплик или фактор репликации настраивается как глобально по умолчанию, так и отдельно в каждом топике. Реплики партициий могут быть лидерами или фолловерами. Традиционно консумеры и продюсеры работают с лидерами, а фолловеры только догоняют лидера.

![Структура топика](https://habrastorage.org/getpro/habr/upload_files/28b/b9a/da6/28bb9ada610275c8b7521a0050cbc6fc.png)

Продюсеры самостоятельно партицируют данные в топиках и сами определяют алгоритм партицирования: он может быть как банальный round-robin и hash-based, так и кастомный. Важно помнить, что очерёдность сообщений гарантируется только для одной партиции.

Продюсер сам выбирает размер батча и число ретраев при отправке сообщений. Протокол Kafka предоставляет гарантии доставки всех трёх семантик: at-most once, at-least once и exactly-once.

- Семантика at-most once означает, что при доставке сообщений нас устраивают потери сообщений, но не их дубликаты. Это самая слабая гарантия, которую реализуют брокерами очередей

- Семантика at-least once означает, что мы не хотим терять сообщения, но нас устраивают возможные дубликаты

- Семантика exactly-once означает, что мы хотим доставить одно и только одно сообщение, ничего не теряя и ничего не дублируя.

Типичная программа-консумер работает так: при запуске внутри неё работает таймер, который периодически поллит новые записи из партиций брокеров. Поллер получает список батчей, связанных с топиками и партициями, из которых читает консумер. Далее полученные сообщения в батчах десериализуются. В итоге консумер, как правило, как-то обрабатывает сообщения.

В конце чтения консумер может закоммитить оффсет — запомнить позицию считанных записей и продолжить чтение новой порции данных. Читать можно как синхронно, так и асинхронно. Оффсет можно коммитить или не коммитить вовсе.

### Отличие Kafka от RabbitMQ

Фундаментальное отличие Kafka от очередей сообщений состоит в том, как сообщения хранятся на брокере и как потребляются консьюмерами.

Сообщения в Kafka не удаляются брокерами по мере их обработки консьюмерами — данные в Kafka могут храниться длительное время. Благодаря этому одно и то же сообщение может быть обработано сколько угодно раз разными консьюмерами и в разных контекстах. В этом кроется главная мощь и главное отличие Kafka от традиционных систем обмена сообщениями.

- Архитектура

    В Kafka используется модель разделенного журнала, которая сочетает в себе подходы очереди сообщений и обмена сообщениями по модели «публикация – подписка».

    В RabbitMQ используется очередь сообщений.

- Хранение сообщений

    Например, в соответствии с политикой, сообщения могут храниться в течение одного дня. Пользователь может настроить это окно хранения.

    В соответствии с принципом подтверждения сообщения удаляются по мере их потребления.

- Для нескольких потребителей

    На одну и ту же тему могут подписаться несколько потребителей, поскольку в Kafka допускается воспроизведение одного и того же сообщения в течение определенного промежутка времени.

    Несколько потребителей не могут получить одно и то же сообщение, поскольку сообщения удаляются по мере их использования.

- Репликация

    Репликация тем осуществляется автоматически, но пользователь может вручную отключить ее.

    Сообщения не реплицируются автоматически, но пользователь может вручную настроить их для репликации.

- Порядок сообщений

    Благодаря архитектуре разделенного журнала каждый потребитель получает информацию по порядку.

    Сообщения доставляются потребителям в порядке их появления в очереди. Если есть конкурирующие потребители, то каждый из них будет обрабатывать свою часть доставленного сообщения.

- Протоколы

    В Kafka используется протокол передачи двоичных данных через TCP.

    Расширенный протокол очереди сообщений (AMQP) с поддержкой плагинов MQTT и STOMP.

### Работа с Kafka в .NET

Для работы с Kafka нам необходимо установить в приложение ([nuget пакет](https://www.nuget.org/packages/Confluent.Kafka/)). Библиотека Confluent предоставляет разработчику высокоуровневые средства для работы с продюсерами, консюмерами и AdminClient, также она совместима со всеми брокерами Apache Kafka версии 0.8 и более поздних версий, Confluent Cloud и Confluent Platform. Также для отладки и тестирования необходимо локально установить брокер Kafka, для этого можно воспользоваться инструкцией в данной [статье](https://habr.com/ru/articles/738874/), в результате у нас будет запущен кластер Kafka, а также Zookeeper и UI для просмотра брокеров и топиков.

![Docker](./Images/Kafka%20-%201.png)

![UI-1](./Images/Kafka%20-%202.png)

![UI-2](./Images/Kafka%20-%203.png)

В рамках данной квартальной цели, для демонстрации возможностей Kafka, было создано учебное приложение доступное по ссылке ([приложение](https://gitlab.com/binarysquad/otus-project/-/tree/feature/TAS-64/src/BlogPlus.Backend/BlogPlus.Notification?ref_type=heads)). В данном проекте есть два приложения, в первом приложении добавлен продюсер, который отправляет сообщения в Kafka, когда требуется отправить письмо по электронной почте при регистрации, а второе приложение их соответственно получает через консюмер и обрабатывает. В обоих приложениях используется библиотека Confluent Kafka.

#### Продюсеры

Начнём рассмотрение возможностей Kafka с изучения продюсера. Для этого нам неоходимо использовать интерфейс IProducer<TKey,TValue>, который представляет собой высокоуровневый API для отправки сообщений. Для того чтобы его инициализировать и сконфигурировать применятся класс ProducerBuilder, который принимает параметр конструктора ProducerConfig, в котором можно задать такие параметры как BootstrapServer, BatchSize, MessageTimeoutMs и.т.д.

``` csharp
using Confluent.Kafka;
using System.Net;

var config = new ProducerConfig
{
    BootstrapServers = "host1:9092",
    ...
};

using (var producer = new ProducerBuilder<Null, string>(config).Build())
{
    ...
}
```

В нашем приложении сделана обёртка над базовым прдюсером для удобной отправки сообщений:

``` csharp
internal sealed class EmailProducer : IKafkaProducer<EmailCreated>
{
    private readonly IProducer<Null, string> _producer;

    public EmailProducer(IOptions<KafkaProducerConfiguration> configuration)
    {
        _producer = new ProducerBuilder<Null, string>(new ProducerConfig
            {
                BootstrapServers = configuration.Value.BootstrapServer
            })
            .Build();
    }

    public async Task Publish(EmailCreated message, CancellationToken cancellationToken)
    {
        var serialize = new Message<Null, string> { Value = JsonSerializer.Serialize(message) };
        var result = await _producer.ProduceAsync(KafkaDefaults.EmailTopic, serialize, cancellationToken);
        if (result.Status == PersistenceStatus.NotPersisted)
            throw new KafkaException(ErrorCode.KafkaStorageError);
    }
}
```

В ProducerConfig мы указываем адресс нашего брокера, этого достаточно для отправки сообщений. Для того, чтобы отправить сообщение(**Value**) используется метод Produce или его асинхронная версия ProduceAsync, в качестве параметров указывается топик, куда необходимо отправить сообщение, а также само сообщение сериализованное в любой удобный формат(JSON, Protobuf, …).

Также при инициализации продюсера и создании сообщения можно указать ключ партиционирования(**Key**). Ключ партицирования может быть любой: числовой, строковый, объект или вовсе пустой как в нашем случае (Указан как **Null**).

В сообщении продюсер может указать время(**Timestamp**), либо за него это сделает брокер в момент приёма сообщения.

Заголовки(**Headers**) выглядят как в HTTP-протоколе — это строковые пары ключ-значение. В них не следует хранить сами данные, но они могут использоваться для передачи метаданных. Например, для трассировки, сообщения MIME-type, цифровой подписи и т.д.

![Структура сообщения](https://habrastorage.org/r/w1560/getpro/habr/upload_files/df1/1f9/2b2/df11f92b22e90cfca1def569f819d51f.png)

Метод Produce возвращает тип DeliveryResult<TKey,TValue>, который содержит в себе данные о переданном сообщении, например свойство **Status** сожержит в себе статус сохранения сообщения, т.е. сохранилось сообщение брокером или нет.

После отправки сообщения оно храниться в брокере, очередь удобно представить как лог: каждая следующая запись добавляется в конец файла и не меняет предыдущих записей. Фактически это очередь FIFO (First-In-First-Out) и Kafka реализует именно эту модель. Начальная позиция первого сообщения в очереди называется log-start offset. Позиция сообщения, записанного последним — log-end offset. Позиция консумера сейчас — current offset. Расстояние между конечным оффсетом и текущим оффсетом консумера называют ***лагом***.

В любых очередях есть выбор между скоростью доставки и расходами на надёжность.

![Семантика доставки](https://habrastorage.org/r/w1560/getpro/habr/upload_files/215/4fc/acc/2154fcaccb5a7874b58cb730d9e91529.png)

- Семантика at-most once означает, что при доставке сообщений нас устраивают потери сообщений, но не их дубликаты. Это самая слабая гарантия, которую реализуют брокерами очередей

- Семантика at-least once означает, что мы не хотим терять сообщения, но нас устраивают возможные дубликаты

- Семантика exactly-once означает, что мы хотим доставить одно и только одно сообщение, ничего не теряя и ничего не дублируя.

#### Консумеры

Для того чтобы получить отправленное сообщение, нам необходимо второе [приложение](https://gitlab.com/binarysquad/otus-project/-/tree/feature/TAS-64/src/BlogPlus.Backend/BlogPlus.Notification/BlogPlus.Notification.MessageQueue?ref_type=heads), в котором реализован IConsumer<TKey,TValue>. Типичная программа-консумер работает так: при запуске внутри неё работает таймер, который периодически поллит новые записи из партиций брокеров. Поллер получает список батчей, связанных с топиками и партициями, из которых читает консумер. Далее полученные сообщения в батчах десериализуются. В итоге консумер, как правило, как-то обрабатывает сообщения.

В конце чтения консумер может закоммитить оффсет — запомнить позицию считанных записей и продолжить чтение новой порции данных. Читать можно как синхронно, так и асинхронно. Оффсет можно коммитить или не коммитить вовсе.

Для реализации интерфейса IConsumer мы используем класс ConsumerBuilder<TKey,TValue> в конструктор которого передаётся параметр ConsumerConfig, используемый для настройки консюмера.

При инициализации ConsumerConfig нам обязательно необходимо установить свойство BootstrapServers, чтобы подключиться к брокеру с которого мы будем получать сообщения, также необходимо указать GroupId т.е. идентификатор группы консюмеров, в которую могут быть объединены несколько консюмеров для параллельного чтения сообщений из очереди, распределение партиций между консумерами в пределах одной группы выполняется автоматически на стороне брокеров.

Также в ConsumerConfig можно указать такие параметры как SessionTimeoutMs, EnableAutoCommit, EnablePartitionEof и.т.д. После инициализации консюмера необходимо подписаться на топик с которого будет происходить чтение сообщений. Это можно сделать с помощью метода Subscribe в который передаётся название топика.

``` csharp
using System;
using System.Threading;
using Confluent.Kafka;

class Program
{
    public static void Main(string[] args)
    {
        var conf = new ConsumerConfig
        {
            GroupId = "test-consumer-group",
            BootstrapServers = "localhost:9092",
            AutoOffsetReset = AutoOffsetReset.Earliest
        };

        using (var c = new ConsumerBuilder<Ignore, string>(conf).Build())
        {
            c.Subscribe("my-topic");

            CancellationTokenSource cts = new CancellationTokenSource();
            Console.CancelKeyPress += (_, e) => {
                e.Cancel = true; // prevent the process from terminating.
                cts.Cancel();
            };

            try
            {
                while (true)
                {
                    try
                    {
                        var cr = c.Consume(cts.Token);
                        Console.WriteLine($"Consumed message '{cr.Value}' at: '{cr.TopicPartitionOffset}'.");
                    }
                    catch (ConsumeException e)
                    {
                        Console.WriteLine($"Error occured: {e.Error.Reason}");
                    }
                }
            }
            catch (OperationCanceledException)
            {
                c.Close();
            }
        }
    }
}
```

В нашем случае это будет выглядеть так - [код консюмера](https://gitlab.com/binarysquad/otus-project/-/blob/feature/TAS-64/src/BlogPlus.Backend/BlogPlus.Notification/BlogPlus.Notification.MessageQueue/Consumers/EmailSource.cs?ref_type=heads). Процесс получения сообщений представляет собой цикл, в котором кажды раз происходит получение сообщений с помощью метода **Consume**. В качестве параметра в этот метод можно передать либо таймаут т.е. максимальный период времени, на который вызов может быть заблокирован, либо CancellationToken.

Метод возвращает тип ConsumeResult<TKey,TValue>, в котором TKey это ключ партиционирования, а TValue наше закодированное сообщение.

После того как мы получили наше сообщение и обработали его, нам необходимо сообщить брокеру что данное сообщение было получено, если при инициализации консюмера свойство EnableAutoCommit было установлено true, то коммит со значением текущего оффсета будет отправлен автоматически, если нет, то необходимо вручную вызвать метод Commit, который в качестве аргумента принимает тип ConsumeResult.

Перечисленных выше шагов хватает для базовой настройки отправки и получения сообщений, однако библиотека Confluent.Kafka обладает большими возможностями для работы с Kafka.

#### Возможности Confluent.Kafka

Помимо API для работы с консюмерами и продюсерами Confluent.Kafka также педоставляет API для взаимодействия с брокером, для этого используется класс ***AdminClient***.

Например с помощью AdminClient мы можем получить все метаданные по группам консюмеров:

``` csharp
using (var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = bootstrapServers }).Build())
{
    var groups = adminClient.ListGroups(TimeSpan.FromSeconds(10));
    Console.WriteLine($"Consumer Groups:");
    foreach (var g in groups)
    {
        Console.WriteLine($"  Group: {g.Group} {g.Error} {g.State}");
        Console.WriteLine($"  Broker: {g.Broker.BrokerId} {g.Broker.Host}:{g.Broker.Port}");
        Console.WriteLine($"  Protocol: {g.ProtocolType} {g.Protocol}");
        Console.WriteLine($"  Members:");
        foreach (var m in g.Members)
        {
            Console.WriteLine($"    {m.MemberId} {m.ClientId} {m.ClientHost}");
            Console.WriteLine($"    Metadata: {m.MemberMetadata.Length} bytes");
            Console.WriteLine($"    Assignment: {m.MemberAssignment.Length} bytes");
        }
    }
 }
```

Для этого нам необходимо инициализировать AdminClient с помощью AdminClientBuilder передав туда данные о брокере BootstrapServers, а затем вызвать метод ListGroups(), этот метод вернём нам все найденные группы, а в данных группах мы уже можем найти всех консюмеров.

Также к примеру мы можем создать топик используя метод CreateTopicsAsync:

``` csharp
using (var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = bootstrapServers }).Build())
{
    try
    {
        await adminClient.CreateTopicsAsync(new TopicSpecification[] { 
                        new TopicSpecification { Name = topicName, ReplicationFactor = 1, NumPartitions = 1 } });
    }
    catch (CreateTopicsException e)
    {
        Console.WriteLine($"An error occurred creating topic {e.Results[0].Topic}: {e.Results[0].Error.Reason}");
    }
}
```

Либо можем изменить оффсет ConsumerGroup:

``` csharp
using (var adminClient = new AdminClientBuilder(new AdminClientConfig { BootstrapServers = bootstrapServers }).Build())
{
    try
    {
        var results = await adminClient.AlterConsumerGroupOffsetsAsync(11);
        Console.WriteLine("Successfully altered offsets:");
        foreach(var groupResult in results)
        {
            Console.WriteLine(groupResult);
        }
    }
}
```

Подробнее ознакомиться с примерами использования AdminClien можно [здесь](https://github.com/confluentinc/confluent-kafka-dotnet/blob/master/examples/AdminClient/Program.cs)

Confluent Kafka также поддерживает работу с такими форматами кодирования данных как Protobuf и Apache Avro. Рассмотрим как отправлять и получать данные в формате Protobuf, для этого необходимо установить nuget пакеты Confluent.SchemaRegistry и Confluent.SchemaRegistry.Serdes.Protobuf. Для работы со схемами используется CachedSchemaRegistryClient, для которого необходимо задать URL-адрес реестра схем, далее с помощью ProtobufSerializer тип будет сериализован согласно полученной схеме.

``` csharp
using Confluent.Kafka;
using Confluent.Kafka.SyncOverAsync;
using Confluent.SchemaRegistry;

var schemaRegistryConfig = new SchemaRegistryConfig
{
    Url = schemaRegistryUrl,
};

using (var schemaRegistry = new CachedSchemaRegistryClient(schemaRegistryConfig))
using (var producer =
    new ProducerBuilder<string, User>(producerConfig)
        .SetValueSerializer(new ProtobufSerializer<User>(schemaRegistry))
        .Build())
{
    User user = new User { Name = text, FavoriteColor = "green", FavoriteNumber = i++ };
    await producer
        .ProduceAsync(topicName, new Message<string, User> { Key = text, Value = user })
        .ContinueWith(task => task.IsFaulted
            ? $"error producing message: {task.Exception.Message}"
            : $"produced to: {task.Result.TopicPartitionOffset}");
}
```

В консюмере необходимо при инициализации установить десириализатор ProtobufDeserializer и указать получаемый тип:

``` csharp
using (var consumer =
        new ConsumerBuilder<string, User>(consumerConfig)
            .SetValueDeserializer(new ProtobufDeserializer<User>()
            .AsSyncOverAsync())
            .SetErrorHandler((_, e) => Console.WriteLine($"Error: {e.Reason}"))
            .Build())
    {
        consumer.Subscribe(topicName);

        try
        {
            while (true)
            {
                try
                {
                    var consumeResult = consumer.Consume(cts.Token);
                    var user = consumeResult.Message.Value;
                    Console.WriteLine($"key: {consumeResult.Message.Key} user name: {user.Name}, favorite number: {user.FavoriteNumber}, favorite color: {user.FavoriteColor}");
                }
                catch (ConsumeException e)
                {
                    Console.WriteLine($"Consume error: {e.Error.Reason}");
                }
            }
        }
        catch (OperationCanceledException)
        {
            consumer.Close();
        }
    }
```

Более подробно о возможностях библиотеки можно узнать [здесь](https://github.com/confluentinc/confluent-kafka-dotnet/tree/master/examples).

### Заключение

На этом отчёт по данной цели закончен. Полезные ссылки:

- [Установка Kafka](https://habr.com/ru/articles/738874/)
- [Офф. документация](https://docs.confluent.io/home/overview.html)
- [Статья на Хабре](https://habr.com/ru/companies/sbermarket/articles/738634/)

## 2. Реализовать сервис с использованием библиотеки акторов Akka.NET

### Обзор Akka.NET

Akka.NET - это набор инструментов и среда выполнения для создания конкурентных, распределенных и отказоустойчивых приложений на .NET и Mono. Поддерживаемый сообществом порт даёт разработчикам C# и F# возможности оригинальной платформы Akka на Java/Scala.

Akka.NET предоставляет:

- Многопоточное поведение без использования низкоуровневых конструкций параллелизма, таких как атомарность или блокировки.
- Прозрачная удаленная связь между системами и их компонентами. Нет необходимости писать или поддерживать сложный сетевой код.
- Кластеризованная архитектура с высокой доступностью, которая является гибкой и масштабируется по требованию.

Все эти функции доступны благодаря единой модели программирования: Akka.NET использует модель **actor** для обеспечения уровня абстракции, который упрощает написание корректных параллельных и распределенных систем. Akka.NET содержит в себе модель actor и предоставляет инструменты для её использования.

Модель actor предоставляет абстракцию, которая позволяет думать о коде с точки зрения коммуникации. Основной характеристикой акторов является то, что они моделируют мир как объекты с отслеживанием состояния, взаимодействующие друг с другом посредством явной передачи сообщений.

Акторы обладают следующими характеристиками:

- Взаимодействуют с помощью асинхронного обмена сообщениями вместо вызовов методов
- Управляют своим собственным состоянием

Отвечая на сообщение, они могут:

- Создавать других (дочерних) субъектов
- Отправлять сообщения другим субъектам
- Останавливать (дочерних) субъектов или самих себя

### Akka Streams

Akka.Streams - это библиотека, созданная поверх Akka.NET, которая позволяет использовать и обрабатывать потенциально бесконечные потоки данных безопасным для типов и ресурсов способом.

Чтобы было проще визуализировать, возьмем пример: у нас есть очередь (RabbitMQ), из которой мы получаем документы, которые нам необходимо проанализировать, трансформировать в структурированные данные и сохранить в базе данных.

``` csharp
var settings = NamedQueueSourceSettings.Create(new DefaultAmqpConnection(), "queue-name");
var rabbitMqSource = AmqpSource.AtMostOnceSource(settings, 16);

IRunnableGraph<NotUsed> graph =
    rabbitMqSource
    .Select(message => ParseDocument<DataItem>(message.Bytes))
    .SelectAsync(parallelism: 1, item => dbContext.InsertAsync(item))
    .To(Sink.ForEach<DataItem>(item => Console.WriteLine($"Inserted: {item}")));
```

Одним из преимуществ использования Akka.Streams является его декларативный синтаксис. Допустим, что мы обнаружили, что наш поток работает медленно, потому что каждый раз мы сохраняем документы последовательно один за другим, что может быть узким местом, если учесть накладные расходы на дисковые и сетевые операции ввода-вывода. Одним из распространенных решений  является применение пакетной обработки. Для этого есть множество операций (в Akka.NET они называются **stage** (этапами)), которые позволяют нам применять различные шаблоны группировки. Пример:

``` csharp
IRunnableGraph<NotUsed> graph =
    rabbitMqSource
    .Select(message => ParseDocument<DataItem>(message.Bytes))
    .GroupedWithin(400, 100.Milliseconds())
    .SelectAsync(parallelism: 1, items => dbContext.InsertManyAsync(items))
    .To(Sink.ForEach<DataItem>(items => Console.WriteLine($"Inserted: {items}")));
```

Код выглядит очень похожим, основное отличие заключается в том, что теперь мы используем метод InsertManyAsync для вставки нескольких элементов одновременно, и что мы добавили stage - GroupedWithin, который просто выдаст пакет элементов, как только накопится 400 элементов или пройдет 100 мс, в зависимости от того, что произойдёт первым.

Akka.Streams имеет множество готовых интеграций с таким технологиям, как Kafka, Azure Queues, RabbitMQ и так далее. Объем кода, который приходится писать и отлаживать, значительно меньше для многих задач потоковой передачи с использованием Akka.Streams.

Помимо этого, Akka.Streams также особенно эффективны и делают системы типа производитель-потребитель более устойчивыми, за счет использования ***backpressure***(противодавления). Когда производители создают события со скоростью значительно превышающей скорость, потребления клиентами или службами, в конечном итоге у приложения закончатся ресурсы и произойдет сбой. Акка.Streams помогает предотвратить эту проблему, позволяя потребителям “оказывать обратное давление”(backpressure) на производителей с помощью так называемой “pull model”(модели вытягивания) - если потребитель не готов получать больше событий от вышестоящего производителя, производитель больше не будет получать запросы “вытягивания” от этого клиента и либо прекратит отправку новых событий или, возможно станет буферизовать/сжимать существующие.

![Схема работы Akka Streams](https://getakka.net/images/graph_stage_conceptual1.png)

Как только потребитель завершит свою работу, он снова начнет получать сообщения от производителя. Этот обмен сообщениями позволяет производителям и потребителям балансировать нагрузку, обрабатывать сообщения настолько быстро, насколько позволяет система, и предотвращает их недоступность в результате чрезмерного использования ресурсов.

### Демонстрация Akka Streams

В рамках данной квартальной цели, для демонстрации возможностей Akka Streams, было создано учебное приложение доступное по ссылке ([приложение](https://gitlab.com/binarysquad/otus-project/-/blob/feature/TAS-64/src/BlogPlus.Backend/BlogPlus.Notification/BlogPlus.Notification.Application/Services/Implementation/EmailLoadingService.cs?ref_type=heads)). В данном приложении реализован сервис, который потребляет поток сообщений из Kafka содержащий эл.письма. С помощью Akka Streams происходит рассылке писем и их сохранение в базе данных.

Для работы с Akka Streams нам необходимо установить в приложение ([nuget пакет](https://www.nuget.org/packages/Akka.Streams)) и настроить источник сообщений для обработки нашим приложением, источником может быть любая коллекция реализующая IEnumearble, IAsyncEnumearble, IObservable или если это EventHandler. Так как у нас в качестве источника данных используется Kafka нам также необходимо добавить пакет Confluent Kafka.

Основные блоки, с помощью которых строится конвейер обработки, делятся на три основные группы:

- Источник данных (***Source***) — стадия обработки с одним выходом.
- Сток (***Sink***) — стадия обработки с одним входом.
- Пропускной пункт (***Flow***) — стадия обработки с одним входом и одним выходом. Здесь происходят функциональные трансформации, причем необязательно в памяти: это может быть, например, обращение к веб-сервису, к каким-то элементам параллелизма, многопоточное.

Для того чтобы начать обрабатывать сообщения нам необходимо определить источник данный с помощью класса ***Source*** и его метода From или его аналогов (FromEvent, FromObservable), например:

``` csharp
using Akka.Streams;
using Akka.Streams.Dsl;

Source<T, NotUsed> source = Source.From(Enumerable.Range(1,100))
```

В нашем случае это интерфейс с методом GetEmail который возвращает поток эл. писем:

``` csharp
using Akka.Streams;
using Akka.Streams.Dsl;

public sealed class EmailLoadingService : BackgroundService
{
    private readonly IEmailSource _emailSource;

    public EmailLoadingService(IEmailSource emailSource)
    {
        _emailSource = emailSource;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var source = Source.From(_emailSource.GetEmail(stoppingToken))
    }
}
```

Источник можно именовать с помощью функции Named() или логгировать с помощью вызова Log(), также можно применить метод Recover() для восстановления потока, это работает так же, как выражение try-catch: оно помечает область, в которой перехватываются исключения. Recover позволяет вам генерировать конечный элемент, а затем завершать поток при сбое в восходящем потоке. Решение о том, какие исключения следует восстановить, выполняется с помощью делегата. Если у исключения нет соответствующего случая, поток завершается ошибкой.

Восстановление может быть полезно, если вы хотите корректно завершить поток при сбое, сообщив при этом нижестоящему потоку о том, что произошел сбой.

Далее нам необходимо определить шаги потоковой обработки с помощью класса ***Flow***, Flow - это набор шагов потоковой обработки, который имеет один открытый вход и один открытый выход, методом Create\<T\>() мы создаём типизированный Flow без указания Source и Sink и можем каким либо образом трансформировать поступающие нам данные, например создадим Flow который принимает целочисленные значения и умножает их на два:

``` csharp
using Akka.Streams;
using Akka.Streams.Dsl;

var flow = Flow.Create<int>().Select(x => x * 2);
```

В нашем приложении создаём первый Flow для рассылки писем с помощью нашего сервиса IEmailSender и преобразования контракта EmailCreated в модель БД:

``` csharp
using Akka.Streams;
using Akka.Streams.Dsl;
using Mapster;
using MongoDB.Driver;

public sealed class EmailLoadingService : BackgroundService
{
    private readonly IEmailSender _emailSender;

    public EmailLoadingService(IEmailSender emailSender)
    {
        _emailSender = emailSender;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var emailSendingFlow = Flow.Create<EmailCreated>()
            .Log("Start email sending.")
            .SelectAsync(5, async email =>
            {
                _logger.Log(LogLevel.Information, "Message received for email sending {@email}", email);
                await _emailSender.SendEmailAsync(email.Adapt<EmailModel>(), stoppingToken);
                return new InsertOneModel<NotificationModel>(email.Adapt<NotificationModel>());
            });
    }
}
```

В данном случае мы используем встроенный пропускной пункт SelectAsync, который позволяет преобразовывать входящие элементы в нужный нам тип, подобно оператору Select в LINQ, и осуществлять нужную нам логику в передаваемом методу делегате. Ещё одним аргументом у SelectAsync является степень параллелизма, у нас он указан как 5, т.е. отправка писем будет осуществляться одновременно в пяти потоках. После рассылки писем мы возвращаем тип InsertOneModel\<NotificationModel\> для последующей обработки другим Flow.

Вторым Flow является пропускной пункт для сохранения отправленных сообщений в БД:

``` csharp
using Akka.Streams;
using Akka.Streams.Dsl;
using Mapster;
using MongoDB.Driver;

public sealed class EmailLoadingService : BackgroundService
{
    private readonly INotificationRepository _notificationRepository;

    public EmailLoadingService(INotificationRepository notificationRepositoryr)
    {
        _notificationRepository = notificationRepository;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var emailSavingFlow = Flow.Create<InsertOneModel<NotificationModel>>()
            .GroupedWithin(5, TimeSpan.FromSeconds(5))
            .SelectAsync(1, async models =>
            {
                await _notificationRepository.SaveBulkAsync(models.ToArray(), stoppingToken);
                return Unit.Default;
            });
    }
}
```

После создания Flow мы выполняем группировку элементов с помощью метода GroupedWithin(), первым аргументом указывается количество элементов в формируемой группе, вторым период времени в течении которого формируется группа. Группа будет сформирована в момент, когда наберётся достаточное количество элементов, либо истечёт выделенный период времени.

В целом, принцип работы тот же самый как у первого Flow, но степень параллелизма указана как 1, что значит выполнение кода только в одном потоке и возвращаемый тип у нас указан как Unit.Default потому что для дальнейшей обработки не предусмотрено и сохранённые сообщения больше не будут использоваться.

Далее нам необходимо определить сток(***Sink***) этап обработки с ровно одним вводом, запрашивающий и принимающий элементы данных, и в нём задать логику того, что будет происходить с данными после их обработки. В простейших случаях Sink может выглядеть так:

``` csharp
using Akka.Streams;
using Akka.Streams.Dsl;

// Sink that aggregates over the stream and returns a Task
// of the final result as its materialized value
Sink.Aggregate<int, int>(0, (sum, i) => sum + i);

// Sink that returns a Task as its materialized value,
// containing the first element of the stream
Sink.First<int>();

// A Sink that consumes a stream without doing anything with the elements
Sink.Ignore<int>();

// A Sink that executes a side-effecting call for every element of the stream
Sink.ForEach<string>(Console.WriteLine);
```

В нашем случае нужды в дальнейшей обработки данных нет, поэтому мы можем игнорировать их:

``` csharp
using Akka.Streams;
using Akka.Streams.Dsl;
using Mapster;
using MongoDB.Driver;

public sealed class EmailLoadingService : BackgroundService
{
    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var sink = Sink.Ignore<Unit>();
    }
}
```

Важно помнить, что даже после построения RunnableGraph(Flow, оба конца которого "подключены" к источнику(Source) и приемнику(Sink) соответственно, и который готов к запуску) путем подключения всех источников, приемников и различных этапов обработки, никакие данные не будут проходить через него до тех пор, пока они не будут материализованы.

Материализация - это процесс распределения всех ресурсов, необходимых для выполнения вычислений, описываемых графом(Graph) (в потоках Akka это часто включает запуск акторов). Благодаря тому, что потоки являются простым описанием конвейера обработки, они неизменяемы, потокобезопасны и доступны для свободного обмена, что означает, что, например, безопасно делиться ими и отправлять их между акторами, чтобы один актор подготовил работу, а затем материализовал ее в другом месте или системе. Для того чтобы материализовать полученный граф необходимо создать материализатор:

``` csharp
using Akka.Streams;
using Akka.Streams.Dsl;
using Mapster;
using MongoDB.Driver;

public sealed class EmailLoadingService : BackgroundService
{
    private readonly ActorSystem _actorSystem;

    public EmailLoadingService(ActorSystem actorSystem)
    {
        _actorSystem = actorSystem;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var materializer = _actorSystem.Materializer();
    }
}
```

А затем материализовать полученный граф, например с помощью метода ToMaterialized(), который принимает одним из аргументов Sink:

``` csharp
using Akka.Streams;
using Akka.Streams.Dsl;
using Mapster;
using MongoDB.Driver;

public sealed class EmailLoadingService : BackgroundService
{
    private readonly ActorSystem _actorSystem;

    public EmailLoadingService(ActorSystem actorSystem)
    {
        _actorSystem = actorSystem;
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var materializer = _actorSystem.Materializer();
        await Source
                .From(_emailSource.GetEmail(stoppingToken))
                .ToMaterialized(sink, Keep.Right)
                .Run(materializer);
    }
}
```

Итоговый код можно посмотреть [здесь](https://gitlab.com/binarysquad/otus-project/-/blob/feature/TAS-64/src/BlogPlus.Backend/BlogPlus.Notification/BlogPlus.Notification.Application/Services/Implementation/EmailLoadingService.cs?ref_type=heads).

### Дополнительные инструменты Akka.Streams

#### Flattening

Flattening - это сглаживание потока последовательностей, например поток задан как поток последовательности элементов, но вместо этого требуется поток элементов, передающий все вложенные элементы внутри последовательностей по отдельности. Операция SelectMany может быть использована для реализации преобразования элементов "один ко многим" с использованием функции маппинга в виде In => IEnumerable\<Out\>.

``` csharp
Source<List<Message>,NotUsed > myData = someDataSource;
Source<Message, NotUsed> flattened = myData.SelectMany(x => x);
```

#### Формирование потока в строгую коллекцию

Например, неограниченная последовательность элементов задана как поток, который необходимо собрать в коллекцию, обеспечивая при этом ограниченность

Распространенная ситуация при работе с потоками - это ситуация, когда нам нужно собрать входящие элементы в коллекцию. Эта операция поддерживается через Sink.Seq, который материализуется в Task\<IEnumerable\<T\>>.

Функция Limit или Take всегда должна использоваться совместно, чтобы гарантировать ограниченность потока, тем самым предотвращая нехватку памяти в программе.

``` csharp
var MAX_ALLOWED_SIZE = 100;

var limited = mySource.Limit(MAX_ALLOWED_SIZE).RunWith(Sink.Seq<string>(), materializer);

var ignoreOverflow = mySource.Take(MAX_ALLOWED_SIZE).RunWith(Sink.Seq<string>(), materializer);
```

#### Удаление элементов

Например, если у мы имеем быстрого производителя и медленного потребителя, мы хотим удалить элементы, чтобы не слишком замедлять работу производителя.

Это можно решить с помощью универсальной операции преобразования скорости Conflate. Conflate можно рассматривать как специальную операцию суммирования, которая сворачивает несколько восходящих элементов в один совокупный элемент, если это необходимо, чтобы скорость восходящего потока не зависела от нисходящего потока.

Когда восходящий поток выполняется быстрее, запускается процесс суммирования Conflate. Наша функция-редуктор просто берет самый свежий элемент. Это показано как простая операция удаления.

``` csharp
var droppyStream = Flow.Create<Message>().Conflate((lastMessage, newMessage) => newMessage);
```

#### Буферы в Akka Streams

Буферы позволяют временно ускорить обработку событий за счет буферизации элементов заданного размера. Когда буфер заполнен, применяется обратное давление. Для создания буфера используется метод Buffer(), где первый аргументом указывается размер буфера, а вторым стратегия при переполнении буфера.

Приведенный ниже пример гарантирует, что 1000 заданий будут удалены из очереди внешней системы и сохранены локально в памяти, что разгрузит внешнюю систему:

``` csharp
// Getting a stream of jobs from an imaginary external system as a Source
Source<Job,NotUsed> jobs = inboundJobsConnector()
jobs.Buffer(1000, OverflowStrategy.Backpressure);
```

Если буфер заполнен, когда доступен новый элемент, эта стратегия создает обратное давление на вышестоящую систему, до тех пор, пока в буфере не освободится место.

Другие стратегии:

- DropTail: если буфер заполнен при поступлении нового элемента, удаляется самый молодой элемент из буфера, чтобы освободить место для нового элемента
- DropHead: если буфер заполнен при поступлении нового элемента, удаляется самый старый элемент из буфера, чтобы освободить место для нового элемента
- DropNew: если буфер заполнен при поступлении нового элемента, новый элемент удаляется
- DropBuffer: если буфер заполнен при поступлении нового элемента, удаляются все буферизованные элементы, чтобы освободить место для нового элемента
- Fail: если буфер заполнен, когда доступен новый элемент, эта стратегия завершает поток с ошибкой

На этом отчёт по данной цели закончен. Полезные ссылки:

- [Статья на хабре](https://habr.com/ru/companies/jugru/articles/418639/)
- [Офф. документация](https://getakka.net/articles/intro/what-is-akka.html)
- [Блог разработчиков](https://petabridge.com/blog/introduction-akkadotnet-streams/)

## 3. Реализовать сервис использующий для полнотекстового поиска Elasticsearch

### Обзор Elasticsearch

Elasticsearch – это распределенный поисковый и аналитический движок на базе Apache Lucene. Вскоре после выпуска в 2010 году Elasticsearch стала самым популярным поисковым движком и обычно используется для таких примеров, как анализ журналов, полнотекстовый поиск, интеллектуальные системы безопасности, бизнес-аналитика и мониторинг текущих процессов. Кроме того, Elasticsearch — это нереляционное хранилище документов в формате JSON, которое разработано на Java и выпущено как проект с открытым исходным кодом в соответствии с условиями лицензии Apache.

Сейчас Elasticsearch — ядро экосистемы Elastic Stack, в состав которой также входят:

- механизм сбора данных и регистрации журналов Logstash;

- платформа аналитики и визуализации Kibana;

- платформа для одноцелевых отправителей данных Beats.

![Экосистема Elastic Stack](https://storage.yandexcloud.net/cloud-www-assets/blog-assets/ru/posts/2021/08/managed-elasticsearch-overview-scheme-1.png.webp)

Elasticsearch — это поиск в режиме реального времени, горизонтально масштабируемый поиск и поддержка многопоточности. Основное преимущество системы — именно в качестве и скорости обработки текста, в том числе при полнотекстовом поиске по всем выражениям во всех документах вашей базы данных.

Основой для работы с текстовыми документами является анализатор. Он представляет собой цепочку последовательных обработчиков. Сначала поступивший в анализатор текст проходит символьные фильтры, которые убирают, добавляют или заменяют отдельные символы в потоке. Например, с помощью символьных фильтров можно заменить арабские цифры (٠ ١٢٣٤٥٦٧٨ ٩) на современные арабские цифры (0123456789), перевести текст в нижний регистр или удалить HTML-теги.

Затем Elasticsearch передает обработанный текст токенизатору, который очищает поток символов от знаков препинания и разбивает по определенным правилам на отдельные слова — токены. В зависимости от токенизатора можно получить как набор слов, так и набор, где будут лишь основы слов (например, корни).

После токенизатора система передает набор слов в один или несколько фильтров токенов, которые могут добавлять, удалять или менять слова в наборе. Например, фильтр стоп-токенов может удалять часто используемые служебные слова, такие как артикли a и the в англоязычном тексте. На выходе из анализатора мы имеем набор токенов, который помещается в индекс. Это позволяет сохранять максимум смысла при минимуме знаков.

Elasticsearch ищет слова из запроса уже по индексу. При этом поисковые индексы можно разделить на сегменты — шарды. На каждом узле (запущенном экземпляре Elasticsearch) может быть размещено несколько сегментов. Каждый узел действует как координатор для делегирования операций правильному сегменту, а перебалансировка и маршрутизация выполняются автоматически.

Elasticsearch — это нереляционное хранилище документов, которое имеет REST API и позволяет работать с данными в формате JSON. Строгая структурированность при этом не является обязательным требованием.

Основные преимущества Elasticsearch — это высокая горизонтальная масштабируемость, легкое управление и отказоустойчивость. Система может быть запущена на десятках или сотнях узлов вместо одного мощного сервера, а добавить в кластер еще один узел можно практически без дополнительных настроек. При этом начать работу с экземпляром узла Elasticsearch очень просто: его установочная версия не требует никаких изменений конфигурации.

Отказоустойчивость реализована таким образом, что индекс Elasticsearch автоматически распределяется по узлам кластера. При сбое одного из узлов индекс перераспределяется на оставшиеся, используя внутренний механизм репликации данных. Кластеры Elasticsearch продолжают работать, даже если возникают аппаратные ошибки типа сбоя узла или неполадок сети.

### Архитектура Elasticsearch

Термин "индекс" довольно перегружен в мире технологий, обычно индексом называют структуру данных в реляционной базе данных (RDBMS), которая связана с таблицей, что повышает скорость операций поиска данных.

Индекс Elasticsearch — это логическое пространство имен, содержащее коллекцию документов, где каждый документ представляет собой набор полей, которые, в свою очередь, являются парами ключ-значение, содержащими ваши данные. Индекс содержит один или более шардов, их совокупность и является хранилищем. Shard в Elasticsearch — это логическая единица хранения данных на уровне базы, которая является отдельным экземпляром Lucene.

Индексы Elasticsearch отличаются от тех, что вы присутствуют в реляционной базе данных. Можно представить кластер Elasticsearch как базу данных, которая может содержать множество индексов, которые вы можете рассматривать как таблицу, и внутри каждого индекса у вас есть много документов.

СУБД => Базы данных => Таблицы => Столбцы/строки
Elasticsearch => Кластеры => Индексы => Сегменты => Документы с парами ключ-значение

Еще одним отличием от реляционных баз данных является то, что вы можете импортировать данные без необходимости предварительного определения схемы. Elasticsearch хранит документы в формате JSON, поэтому такая модель хранения данных называется документоориентированной.

Elasticsearch предоставляет API на основе RESTful JSON для взаимодействия с документами. Мы можем индексировать, искать, обновлять и удалять документы, отправляя HTTP-запросы на соответствующие эндпоинты кластера. Эти CRUD-подобные операции могут выполняться на уровне отдельного документа или на уровне самого индекса. Также клиентские библиотеки, зависящие от языка, которые можно использовать вместо прямого REST, в нашем случае мы будем рассматривать клиентскую библиотеку для .NET.

В следующем примере создается документ в индексе с именем playwrights с присвоенным document_id равным 1. Обратите внимание, что нам не нужно создавать какие-либо схемы или предварительную конфигурацию; мы просто вставляем наши данные.

``` csharp
POST /playwrights/_doc/1
{
  "firstname": "William",
  "lastname": "Shakespeare"
}
```

В дальнейшем мы можем добавлять документы и поля по своему усмотрению, что нелегко сделать с реляционной базой данных.

``` csharp
POST /playwrights/_doc/2
{
  "firstname": "Samuel",
  "lastname": "Beckett",
  "year_of_birth": 1906
}
```

Теперь мы можем запросить все документы, используя эндпоинт для поиска.

``` csharp
GET /playwrights/_search
{
    "query": {
        "match_all": {}
    }
}
```

Или мы можем запросить конкретный год рождения.

``` csharp
GET /playwrights/_search
{
    "query": {
        “match": {
            “year_of_birth": 1906
        }
    }
}
```

### .NET Client для Elasticsearch

В рамках данной квартальной цели, было создано учебное приложение доступное по ссылке ([приложение](https://gitlab.com/femto8032/2023Q4/-/tree/main/ELK/ELK?ref_type=heads)). В данном приложении реализован веб сервис, который взаимодействует с базой данных Elasticsearch, через клиентскую библиотеку для .NET. С помощью методов minimal api мы взаимодействуем с базой данных, отправляя ей запросы, также сервис создаёт индекс и заполняет  данными БД. В качестве схемы БД используется модель поста из интернет блога.

Перед тем как приступить к рассмотрению приложения нам необходимо развернуть Elasticsearch и Kibana для возможности визуального просмотра БД. Для этого можно воспользоваться докером образом из hub.docker.com, инструкцию по развёртыванию можно найти ([здесь](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html)) главное установить образ именно с hub.docker.com.

Сначала необходимо создать сеть:

``` docker
docker network create elastic
```

Далее добавляем первый узел:

``` docker
docker run --name es01 --net elastic -p 9200:9200 -it elasticsearch:8.11.3
```

Если контейнер успешно поднялся, в консоле должно появиться сообщение о том, что все настройки аутентификации успешно применены и показан созданный пароль и токены:

![Аутентификации](./Images/ELK-1.png)

Полученные учётные данные нам необходимы для авторизации в Elasticsearch, добавления новых узлов в кластер и интеграции с кибаной. Можно добавить ещё несколько узлов в кластер, для этого нам необходимо скопировать enrollment token(время действия 30 минут) и поднять контейнер указав этот токен:

``` docker
docker run -e ENROLLMENT_TOKEN="eyJ2ZXIiOiI4LjExLjMiLCJhZHIiOlsiMTcyLjIwLjAuMjo5MjAwIl0sImZnciI6ImFhNzBjZjIxYTNmMGU4YWY5Y2Y0NmZhNThmNzE0NjE5YzhmZjEzMDkzNzJjZDI4ZTUyMjJlOWIwNzIwNjNkZGYiLCJrZXkiOiJCejdxcFl3QnpMMERyV1NoSTl5Mzo2VDN5MHg2Y1NVbWo1UG5IV0c0UHZnIn0=" --name es02 --net elastic -it -m 1GB docker.elastic.co/elasticsearch/elasticsearch:8.11.3
```

Если токен был просрочен можно сгенерировать новый коммандой:

``` docker
docker exec -it es01 /usr/share/elasticsearch/bin/elasticsearch-create-enrollment-token -s kibana
```

После этого нам необходимо поднять контейнер с кибаной, следующей командой:

``` docker
docker run --name kib01 --net elastic -p 5601:5601 kibana:8.11.3
```

Если всё прошло успешно в консоле должна появиться ссылка для настройки кибаны:

![Кибана](./Images/ELK-2.png)

Переходим по ней, вставляем enrollment token и мы успешно авторизовываемся в Elasticsearch. После всех действий у нас должно быть запущено три контейнера:

![Докер](./Images/ELK-3.png)

На этом подготовка инфраструктуры закончена и можно переходить к приложению. Для того чтобы воспользоваться стандартным клентом для платформы .NET нам необходимо установить nuget пакеты - [Elasticsearch.Net](https://www.nuget.org/packages/Elasticsearch.Net) и [NEST](https://www.nuget.org/packages/NESTt) в наше приложение. Далее выносим в переменные среды полученные при создании контейнера учётные данные - Username, Password, Fingerprint они необходимы для успешного подключения к Elasticsearch, так как порт у нас был указан как 9200 наш url будет выглядеть так - [https://localhost:9200].

Для работы с БД нам необходима схема, так будет выглядеть схема для поста и тега:

``` csharp
/// <summary>
/// Модель поста.
/// </summary>
public sealed record Post
{
    public Guid Id { get; init; } = Guid.NewGuid();
    
    public string Title { get; init; }
    
    public string Content { get; init; }
    
    public DateTimeOffset PublishedAt { get; init; }
    
    [Nested]
    public IReadOnlyList<Tag> Tags { get; init; }
}
```

``` csharp
/// <summary>
/// Модель тега.
/// </summary>
public sealed record Tag
{
    public Guid Id { get; init; } = Guid.NewGuid();

    public string Name { get; init; }
}
```

Теперь нам необходимо создать индекс для хранения документов и заполнить его тестовыми данными, на которых можно проиллюстрировать запросы. Для этого сперва добавляем зависимость IElasticClient это наш высокоуровневый клиент для работы с базой:

``` csharp
builder.Services.AddSingleton<IElasticClient>(provider =>
{
    var elasticOptions = provider.GetRequiredService<IOptions<ElasticOptions>>().Value;
    var setting = new ConnectionSettings(new Uri(elasticOptions.ConnectionUri))
        .BasicAuthentication(elasticOptions.Username, elasticOptions.Password)
        .CertificateFingerprint(elasticOptions.Fingerprint)
        .DefaultIndex(ElkDefaults.PostIndex)
        .EnableApiVersioningHeader();
    
    return new ElasticClient(setting);
});
```

После того как клиент настроен создадим сервис для создания индекса и заполнения тестовыми данными. При создании индекса достаточно указать его имя, дальнейшие настройки опциональны, в нашем случае настроим маппинг сущности Post, а именно маппинг вложенного списка тегов, нам необходимо указать свойство Tags как Nested т.е. вложенное, подробнее почитать про nested типы можно [здесь](https://opster.com/guides/elasticsearch/data-architecture/elasticsearch-nested-field-object-field/):

``` csharp
var createIndexResponse = await elasticClient.Indices.CreateAsync(ElkDefaults.PostIndex, c => c
            .Map<Post>(ms => ms
                .AutoMap<Post>()
                .Properties(p => p
                    .Nested<Tag>(n => n
                        .Name(post => post.Tags)))));
```

Также укажем анализатор "russian" для поля Content:

``` csharp
var createIndexResponse = await elasticClient.Indices.CreateAsync(ElkDefaults.PostIndex, c => c
            .Map<Post>(ms => ms
                .AutoMap<Post>()
                .Properties(p => p
                    .Nested<Tag>(n => n
                        .Name(post => post.Tags))
                    .Text(t => t
                        .Name(p => p.Content)
                        .Analyzer("russian")))), stoppingToken);
```

Анализ текста - это процесс преобразования неструктурированного текста, такого как текст электронного письма или описание продукта, в структурированный формат, оптимизированный для поиска.

Elasticsearch выполняет анализ текста при индексации или поиске текстовых полей. Если индекс не содержит текстовых полей, дальнейшая настройка не требуется.

Однако, если используются текстовые поля или результаты поиска по тексту не соответствуют ожиданиям, часто может помочь настройка анализа текста.

Анализаторы нужны, чтобы преобразовать исходный текст в набор токенов.
Анализаторы состоят из одного Tokenizer и нескольких необязательных TokenFilters. Tokenizer может предшествовать нескольким CharFilters.

Tokenizer разбивают исходную строку на токены, например, по пробелам и символам пунктуации. TokenFilter может изменять токены, удалять или добавлять новые, например, оставлять только основу слова, убирать предлоги, добавлять синонимы. CharFilter — изменяет исходную строку целиком, например, вырезает html теги. В Elasticsearch есть несколько стандартных анализаторов. Например, анализатор russian.

После настройки индекса заполним БД данным с помощью Bulk вставки документов:

``` csharp
var response = await elasticClient
    .BulkAsync(descriptor => descriptor
        .Index(ElkDefaults.PostIndex)
        .UpdateMany(TestPosts.GetTestPosts(), (ud, d) => ud
            .Doc(d)
            .DocAsUpsert(true)), stoppingToken);
```

Если всё прошло успешно мы сможем увидеть в кибане созданный индекс с названием "post" и добавленные документы

![Индекс](./Images/ELK-4.png)

После того как все приготовления завершены можно перейти к рассмотрению возможностей клиента, сперва рассмотрим как создавать документ. Для создания используется метод Index(), либо его асинхронная версия IndexAsync(), первым аргументом необходимо указать сохраняемы документ, на основе которого будет создан тип в индексе, затем указывается делегат, в который необходимо обязательно передать название индекса. Метод возвращает тип IndexResponse, который по сути является ответом от сервера на http запрос, в данном случае будет выполняться PUT запрос, в ответе можно посмотреть успешность запроса, созданный идентификатор или ошибку полученную от сервера.

``` csharp
var indexResponse = await client
    .IndexAsync(request.Adapt<Post>(),
        descriptor => descriptor.Index(ElkDefaults.PostIndex).OpType(OpType.Create), context.RequestAborted);
```

Если у сохраняемого документа есть поле id, то если у него задано значение оно будет сохранено в БД как ключ _id, если не задано то ключ будет сформирован базой данных. С помощью эндпоинта SetPost можно попробовать создать пост.

Также сохранённый документ можно получить по ключу _id, для этого необходимо использовать метод GetAsync\<T\>(), здесь также указываем индекс.

Метод вернёт тип GetResponse, который будет содержать те же метаданные, что и IndexResponse, в добавок к этому ответ будет содержать поле Source, из которого можно получить десериализованное значение нашей сущности.

``` csharp
var post = await client.GetAsync<Post>(id, descriptor => descriptor.Index(ElkDefaults.PostIndex));
        return post.Source;
```

Для того чтобы посмотреть на возвращаемые значения сущностей, необходимо воспользоваться эндпоинтом GetPost, в качестве ключа можно указать любой сохранённый в БД ключ от тестовых данных.

Если нам нужно вернуть только значения определённых полей, а не всю сущность, то при запросе документа по ключу нам необходимо указать список включаемых полей SourceIncludes, либо исключаемых SourceExcludes, также можно вообще не возвращать тело документа SourceEnabled(false).

``` csharp
var title = (await client.GetAsync<Post>(id, descriptor => descriptor.SourceIncludes(s => s.Title)))?.Source.Title;
```

В данном случае мы ищем документ по ключу, а затем получаем только поле Title искомого поста, для этого можно использовать эндпоинт GetTitle. Если поиск осуществляется не по ключу, можно использовать метод StoredFields для указания нужных полей:

``` csharp
var searchResponse = _client.Search<Project>(s => s
    .StoredFields(sf => sf
        .Fields(
            f => f.Name,
            f => f.StartedOn,
            f => f.Branches
        )
    )
    .Query(q => q
        .MatchAll()
    )
);
```

Либо использовать Source filtering:

``` csharp
var searchResponse = _client.Search<Project>(s => s
    .Source(sf => sf
        .Includes(i => i 
            .Fields(
                f => f.Name,
                f => f.StartedOn,
                f => f.Branches
            )
        )
        .Excludes(e => e 
            .Fields("num*") 
        )
    )
    .Query(q => q
        .MatchAll()
    )
);
```

Подробнее об их отличия можно почитать [здесь](https://www.elastic.co/guide/en/elasticsearch/client/net-api/7.17/returned-fields.html)

Далее попробуем отсортировать посты по дате публикации, для этого воспользуемся эндпоинтом GetSortedPosts, в котором все посты возвращаются отсортированными по убыванию или возрастанию в зависимости от параметра запроса:

``` csharp
var postsRequest = await client
    .SearchAsync<Post>(descriptor => descriptor
        .Index(ElkDefaults.PostIndex)
        .Query(q => q.MatchAll())
        .Sort(sd => isDescendingOrder
            ? sd.Descending(p => p.PublishedAt)
            : sd.Ascending(p => p.PublishedAt)));
```

В данном случае осуществляем поиск методом SearchAsync(), в запросе Query указываем MatchAll для выборки всех документов, далее с помощью Sort сортируем документы, при необходимости можно задать количество документов которые необходимо вернуть, с помощью метода Size().

Эндпоинт GetRangedPostsByDate выполняет ранжирование документов по дате, переданной из параметра запроса, возвращаются документы с датой публикации равной или большей переданной методу:

``` csharp
var postsRequest = await client
    .SearchAsync<Post>(descriptor => descriptor
        .Index(ElkDefaults.PostIndex)
        .Query(q => q
            .DateRange(d => d
                .Field(s => s.PublishedAt)
                .GreaterThanOrEquals(DateMath
                    .Anchored(dateTime.DateTime)
                    .RoundTo(DateMathTimeUnit.Day)))));
```

Для ранжирования дат применяется метод DateRange, для чисел Range, далее указывается поле для ранжирования и диапазон. Также можно указывать несколько диапазонов, например так:

``` csharp
results = _client.Search<Book>(s => s
    .Query(q => q
        .MatchAll()
    )
    .Aggregations(a => a
        .Range("pageCounts", r => r
            .Field(f => f.PageCount)
            .Ranges(r => r.From(0),
                    r => r.From(200).To(400),
                    r => r.From(400).To(600),
                    r => r.From(600)
            )
        )
    )
);
```

Далее рассмотрим поиск по вложенным типам, в нашем случае это будет поиск по тегам:

``` csharp
var postsRequest = await client
    .SearchAsync<Post>(descriptor => descriptor
        .Index(ElkDefaults.PostIndex)
        .Query(q => q
            .Nested(n => n
                .Path(p => p.Tags)
                .Query(q => q
                    .Term(t => t
                        .Field(f => f.Tags[0].Name)
                        .Value(tag)))
                .IgnoreUnmapped())));
```

Для поиска необходимо в запросе Query вызвать метод Nested, в котором нужно определить Path, в нашем случае это свойство Tags, далее можно осуществить поиск внутри списка тэгов с помощью метода Term. Для указания поля Name сущности Tag можно взять первый элемент массива и получить имя нужного свойства, в нашем случае это Name, а в методе Value будет значение по которому будет осуществляться поиск. Если бы свойство Tags не было вложенным то запрос выглядел бы так:

``` csharp
var postsRequest = await client
    .SearchAsync<Post>(descriptor => descriptor
        .Index(ElkDefaults.PostIndex)
        .Query(q => q
            .Term(t => t
                .Field(msg => msg.Tags[0].Name)
                .Value(tag))));
```

Для полнотекстового поиска воспользуемся эндпоинтом GetMatchedPost:

``` csharp
var postsRequest = await client
    .SearchAsync<Post>(descriptor => descriptor
        .Index(ElkDefaults.PostIndex)
        .Query(q => q
            .Match(m => m
                .Field(f => f.Content)
                .Query(search))));
```

Для этого в методе Match необходимо указать поле по которому будет вестись поиск, а в методе Query указать искомое слово. Так как выше мы уже задали анализатор russian для данного поля, то поиск будет осуществляться с учётом морфологии. Например если искать слово "история", то будет найдено три поста Post1, Post2, Post3 то есть слово "истории" в Post1 будет также подходить под условия поиска, при стандартном анализаторе в выдаче мы бы нашли только Post2 и Post3.

На этом отчёт по данной цели закончен. Полезные ссылки:

- [Учебное приложение](https://gitlab.com/femto8032/2023Q4/-/tree/main/ELK/ELK?ref_type=heads)
- [Офф. документация](https://www.elastic.co/guide/en/elasticsearch/client/net-api/7.17/introduction.html)
- [Статья на хабре](https://habr.com/ru/articles/489924/)
- [Статья на хабре](https://habr.com/ru/articles/280488/)
- [Пример приложения](https://yamannasser.medium.com/simplifying-elasticsearch-crud-with-net-core-a-step-by-step-guide-25c86a12ae15)
