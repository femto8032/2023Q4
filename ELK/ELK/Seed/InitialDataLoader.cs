using ELK.Defaults;
using ELK.Models;
using Nest;

namespace ELK.Seed;

/// <summary>
/// Сервис инициализации базы данных.
/// </summary>
public sealed class InitialDataLoader(IServiceProvider serviceProvider) : BackgroundService
{
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        using var scope = serviceProvider.CreateScope();
        
        var elasticClient = scope.ServiceProvider.GetRequiredService<IElasticClient>();
        var existsResponse =  await elasticClient.Indices.ExistsAsync(ElkDefaults.PostIndex, ct: stoppingToken);
        if(existsResponse.Exists)
            return;
        
        var createIndexResponse = await elasticClient.Indices.CreateAsync(ElkDefaults.PostIndex, c => c
            .Map<Post>(ms => ms
                .AutoMap<Post>()
                .Properties(p => p
                    .Nested<Tag>(n => n
                        .Name(post => post.Tags))
                    .Text(t => t
                        .Name(p => p.Content)
                        .Analyzer("russian")))), stoppingToken);
        
        if (!createIndexResponse.IsValid)
            throw new Exception($"Creating index error: {createIndexResponse.ServerError.Error.Reason}");
        
        var response = await elasticClient
            .BulkAsync(descriptor => descriptor
                .Index(ElkDefaults.PostIndex)
                .UpdateMany(TestPosts.GetTestPosts(), (ud, d) => ud
                    .Doc(d)
                    .DocAsUpsert(true)), stoppingToken);

        if (!response.IsValid)
            throw new Exception($"Data Loading error: {response.ServerError.Error.Reason}");
    }
}