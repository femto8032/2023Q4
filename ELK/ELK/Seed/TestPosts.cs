using ELK.Models;

namespace ELK.Seed;

public static class TestPosts
{
    /// <summary>
    /// Данные для заполнения пустой базы данных.
    /// </summary>
    /// <returns>Тестовые данные.</returns>
    public static IEnumerable<Post> GetTestPosts()
    {
        yield return new Post
        {
            Title = "Post1",
            Content = "Пост о истории произошедшей...",
            PublishedAt = DateTimeOffset.Now,
            Tags = new List<Tag>
            {
                new Tag
                {
                    Name = "advertisement"
                },
                new Tag
                {
                    Name = "company"
                }
            }
        };
        yield return new Post
        {
            Title = "Post2",
            Content = "История о людях...",
            PublishedAt = DateTimeOffset.Now.AddHours(2),
            Tags = new List<Tag>
            {
                new Tag
                {
                    Name = "humor"
                }
            }
        };
        yield return new Post
        {
            Title = "Post3",
            Content = "История написанная...",
            PublishedAt = DateTimeOffset.Now.AddDays(-2),
            Tags = new List<Tag>
            {
                new Tag
                {
                    Name = "advertisement"
                },
                new Tag
                {
                    Name = "production"
                }
            }
        };
        yield return new Post
        {
            Title = "Post4",
            Content = "События связанные...",
            PublishedAt = DateTimeOffset.Now.AddDays(1),
            Tags = new List<Tag>
            {
                new Tag
                {
                    Name = "daily"
                }
            }
        };
        yield return new Post
        {
            Title = "Post5",
            Content = "Сегодня поговорим о...",
            PublishedAt = DateTimeOffset.Now,
            Tags = new List<Tag>
            {
                new Tag
                {
                    Name = "news"
                }
            }
        };
        yield return new Post
        {
            Title = "Post6",
            Content = "Сегодняшнее выступление было...",
            PublishedAt = DateTimeOffset.Now,
            Tags = new List<Tag>
            {
                new Tag
                {
                    Name = "news"
                }
            }
        };
        yield return new Post
        {
            Title = "Post7",
            Content = "Очевидные плюсы...",
            PublishedAt = DateTimeOffset.Now.AddDays(4),
            Tags = new List<Tag>
            {
                new Tag
                {
                    Name = "science"
                }
            }
        };
        yield return new Post
        {
            Title = "Post8",
            Content = "Результаты тестирования показали...",
            PublishedAt = DateTimeOffset.Now,
            Tags = new List<Tag>
            {
                new Tag
                {
                    Name = "computers"
                }
            }
        };
    }
}