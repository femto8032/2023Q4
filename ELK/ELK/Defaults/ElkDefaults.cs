using ELK.Models;

namespace ELK.Defaults;

public static class ElkDefaults
{
    /// <summary>
    /// Наименование индекса для хранения постов.
    /// </summary>
    public static readonly string PostIndex = nameof(Post).ToLower();
}