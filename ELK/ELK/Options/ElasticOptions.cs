using System.ComponentModel.DataAnnotations;

namespace ELK.Options;

public sealed record ElasticOptions
{
    [Required]
    public string ConnectionUri { get; init; }
    
    [Required]
    public string Username { get; init; }
    
    [Required]
    public string Password { get; init; }
    
    [Required]
    public string Fingerprint { get; init; }
}