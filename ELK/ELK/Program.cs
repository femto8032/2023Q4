using Elasticsearch.Net;
using ELK.Defaults;
using ELK.Models;
using ELK.Options;
using ELK.Seed;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Nest;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHostedService<InitialDataLoader>();
builder.Services
    .AddOptions<ElasticOptions>()
    .BindConfiguration(nameof(ElasticOptions))
    .ValidateDataAnnotations()
    .ValidateOnStart();

builder.Services.AddSingleton<IElasticClient>(provider =>
{
    var elasticOptions = provider.GetRequiredService<IOptions<ElasticOptions>>().Value;
    var setting = new ConnectionSettings(new Uri(elasticOptions.ConnectionUri))
        .BasicAuthentication(elasticOptions.Username, elasticOptions.Password)
        .CertificateFingerprint(elasticOptions.Fingerprint)
        .DefaultIndex(ElkDefaults.PostIndex)
        .EnableApiVersioningHeader();
    
    return new ElasticClient(setting);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

// Поиск поста по идентификатору
app.MapGet("/post/{id:guid}", async (
        [FromRoute] Guid id,
        [FromServices] IElasticClient client) =>
    {
        var post = await client.GetAsync<Post>(id, descriptor => descriptor.Index(ElkDefaults.PostIndex));
        return post.Source;
    })
    .WithName("GetPost")
    .WithOpenApi();

// Поиск названия поста по идентификатору
app.MapGet("/title/{id:guid}", async (
        [FromRoute] Guid id,
        [FromServices] IElasticClient client) =>
    {
        var title = (await client.GetAsync<Post>(id, descriptor => descriptor.SourceIncludes(s => s.Title)))?.Source.Title;
        return title;
    })
    .WithName("GetTitle")
    .WithOpenApi();

// Список отсортированных по дате постов
app.MapGet("/sorted-posts", async (
        [FromServices] IElasticClient client,
        [FromQuery] bool isDescendingOrder = true) =>
    {
        var postsRequest = await client
            .SearchAsync<Post>(descriptor => descriptor
                .Index(ElkDefaults.PostIndex)
                .Query(q => q.MatchAll())
                .Sort(sd => isDescendingOrder
                    ? sd.Descending(p => p.PublishedAt)
                    : sd.Ascending(p => p.PublishedAt)));

        return postsRequest.IsValid
            ? Results.Ok(postsRequest.Documents)
            : Results.BadRequest();
    })
    .WithName("GetSortedPosts")
    .WithOpenApi();

// Список ранжированных по дате постов
app.MapGet("/ranged-posts", async (
        [FromServices] IElasticClient client,
        [FromQuery] DateTimeOffset dateTime) =>
    {
        var postsRequest = await client
            .SearchAsync<Post>(descriptor => descriptor
                .Index(ElkDefaults.PostIndex)
                .Query(q => q
                    .DateRange(d => d
                        .Field(s => s.PublishedAt)
                        .GreaterThanOrEquals(DateMath
                            .Anchored(dateTime.DateTime)
                            .RoundTo(DateMathTimeUnit.Day)))));

        return postsRequest.IsValid
            ? Results.Ok(postsRequest.Documents)
            : Results.BadRequest();
    })
    .WithName("GetRangedPostsByDate")
    .WithOpenApi();

// Поиск поста по вложенному массиву тэгов
app.MapGet("/post-by-tag", async (
        [FromServices] IElasticClient client,
        [FromQuery] string tag) =>
    {
        var postsRequest = await client
            .SearchAsync<Post>(descriptor => descriptor
                .Index(ElkDefaults.PostIndex)
                .Query(q => q
                    .Nested(n => n
                        .Path(p => p.Tags)
                        .Query(q => q
                            .Term(t => t
                                .Field(f => f.Tags[0].Name)
                                .Value(tag)))
                        .IgnoreUnmapped())));

        return postsRequest.IsValid
            ? Results.Ok(postsRequest.Documents)
            : Results.BadRequest();
    })
    .WithName("GetPostByTag")
    .WithOpenApi();

// Полнотекстовый поиск постов
app.MapGet("/matched-post", async (
        [FromServices] IElasticClient client,
        [FromQuery] string search) =>
    {
        var postsRequest = await client
            .SearchAsync<Post>(descriptor => descriptor
                .Index(ElkDefaults.PostIndex)
                .Query(q => q
                    .Match(m => m
                        .Field(f => f.Content)
                        .Query(search))));

        return postsRequest.IsValid
            ? Results.Ok(postsRequest.Documents)
            : Results.BadRequest();
    })
    .WithName("GetMatchedPost")
    .WithOpenApi();

// Создать пост
app.MapPost("/post", async (
            [FromBody] Post request,
            [FromServices] IElasticClient client,
            LinkGenerator linkGenerator,
            HttpContext context) =>
    {
        var indexResponse = await client
            .IndexAsync(request.Adapt<Post>(),
                descriptor => descriptor.Index(ElkDefaults.PostIndex).OpType(OpType.Create), context.RequestAborted);

        if (!indexResponse.IsValid)
            return Results.BadRequest();

        var customerLink = linkGenerator.GetUriByName(
            context, "GetPost",
            new { id = indexResponse.Id }
        );

        return Results.Created(customerLink, null);
    })
    .WithName("SetPost")
    .WithOpenApi();

app.Run();