namespace ELK.Models;

/// <summary>
/// Модель тега.
/// </summary>
public sealed record Tag
{
    public Guid Id { get; init; } = Guid.NewGuid();

    public string Name { get; init; }
}