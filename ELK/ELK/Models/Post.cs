using Nest;

namespace ELK.Models;

/// <summary>
/// Модель поста.
/// </summary>
public sealed record Post
{
    public Guid Id { get; init; } = Guid.NewGuid();
    
    public string Title { get; init; }
    
    public string Content { get; init; }
    
    public DateTimeOffset PublishedAt { get; init; }
    
    [Nested]
    public IReadOnlyList<Tag> Tags { get; init; }
}